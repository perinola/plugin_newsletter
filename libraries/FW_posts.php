<?php
/**
 * Todos los posts de los diferentes formularios
 */
class FW_posts {
	
	var $current_website;
	var $company;
	var $tel;
	var $contact_email;
	var $top_image_route;
	var $FW;
	function __construct() {
		
		$this->FW						=& get_instance();
		$this->FW->load->library('email');
		
		//Datos generales del sitio
		$this->current_website			= $_SERVER['HTTP_HOST'];
		$this->company					= $this->FW->fw_resource->request('RESOURCE_COMPANY_NAME');
		$this->tel						= $this->FW->fw_resource->request('RESOURCE_COMPANY_PHONE');
		$this->contact_email			= $this->FW->fw_resource->request('RESOURCE_CONTACT_EMAIL');
		$this->email_header				= $this->FW->fw_resource->request('RESOURCE_EMAIL_IMAGE_ROUTE');
		
		$this->top_image_route			= base_url($this->email_header);
		
		//Validar formularios
		$this->FW->load->library('user_agent');
	}
	
	/**
	 * Funciones de cada post
	 */
	//Enviar bolet�n electr�nico
	public function send_newsletter($toarray, $subject, $body, $image){
		
		foreach($toarray as $to)
		if($this->FW->input->post()):
			//Establecer par�metros
			$this->FW->email->from($this->FW->input->post('inputEmail'), $this->FW->input->post('inputName'));
			$this->FW->email->to($to); 
			$this->FW->email->cc($this->FW->input->post('inputEmail'));	
			$this->FW->email->subject($subject);
			
			$html_message = $this->_html_newsletter_body_template($this->current_website, $body, $this->company, $this->tel, $this->contact_email, $image);
			$this->FW->email->message($html_message);
			$this->FW->email->send();
		endif;
	}
	/**
	 * Template del HTML a enviar por bolet�n electr�nico
	 */
	private function _html_newsletter_body_template($current_website, $body_content, $company, $tel, $contact_email, $newsletter_image){
		$html_code = '<html>
						<head><title>'.$current_website.'</title></head>
						<body style="font-family:Arial, Helvetica; sans-serif;">
							<table width="550" style="background-color:#EEE;width:526px;font-size:12px;">
							<tr>
								<td>
									<table width="550" style="border-color:#dbdbdb;border-style:solid;border-width:1px;width:526px;font-size:12px; font-family:Arial, Helvetica; sans-serif;">
										<tr style="line-height:20px;"><td style="font-size:14px;"><img src="'.$this->top_image_route.'" width="550" height="60" style="padding:0px;margin:0px;" /></td></tr>';
		$html_code .= (!empty($newsletter_image))?'
										<tr>
											<td style="padding-left:10px;padding-top:10px;padding-bottom:10px"><img src="'.$newsletter_image.' /></td>
										</tr>':'';
		$html_code .= '				
										<tr>
											<td style="padding-left:10px;padding-top:10px;padding-bottom:10px">'.$body_content.'</td>
										</tr>
									</table>
								</td>
							</tr>
							</table>
							<p style="font-size:11px;color:#505050;">La informaci&oacute;n contenida en este mensaje es privada y confidencial. Si la ha recibido por error, por favor proceda a notificar al remitente y eliminarla de su sistema.</p>
							<p style="font-size:12px;color:#505050;">Atentamente,</p>
							<p style="font-size:12px;color:#505050;">'.$company.'</p>
							<p style="font-size:11px;color:#505050;">Tel. '.$tel.'</p>
							<p style="font-size:9px;color:#707070;">Custom Site desarrollado por <a href="http://www.grupoperinola.com">Perinola</a></p>
						</body>
					</html>';
		return $html_code;
	}
}
