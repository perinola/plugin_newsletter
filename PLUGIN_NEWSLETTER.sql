-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 14, 2013 at 11:22 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `c04305_LABS_EWS`
--

-- --------------------------------------------------------

--
-- Table structure for table `PLUGIN_NEWSLETTER`
--

CREATE TABLE `PLUGIN_NEWSLETTER` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NEWSLETTER_SUBJECT` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NEWSLETTER_IMAGE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `NEWSLETTER_RECIPIENTS` text COLLATE utf8_unicode_ci,
  `NEWSLETTER_CONTENT` text COLLATE utf8_unicode_ci,
  `NEWSLETTER_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
