<div id="content" class="container-fluid">
	<div class="page-header">
		<h1> <?php echo $page_title; ?><small></small></h1>
	</div>
	<?php if(!empty($create_new_row)):?>
	<div class="row-fluid">
		<div class="span12 well">
			<div class="row-fluid">
				<div class="span2">
					<a class="btn btn-primary" href="<?=base_url("cms/".$current_plugin."/create_new_row")?>"><?=$create_new_row?></a>
				</div>
				<div class="span3">
					<form class="form-inline">
						<label>Filtrar por mes </label>
						<?php		
						$jsm = 'onchange="month_filteroptions() " id="MONTH_FILTER"';
						echo form_dropdown('MONTH_FILTER', $month_opts,$current_month,$jsm);?>
						</form>
				</div>
				<div class="span2">
					<form class="form-inline">
						<label>del </label>
					<?php
						
						$jsa = 'onchange="year_filteroptions() " id="YEAR_FILTER"';
						echo form_dropdown('YEAR_FILTER', $years_opts,$current_year,$jsa);?>
					</form>
				</div>
				<div class="span5">
					<?php echo $pagination;?>
				</div>
			</div>
		</div>
	</div>
	<?php endif?>
	<div class="row-fluid">
		<div class="span12">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<?php foreach($header as $i => $th):?>
						<?php if($i > 0):?>
						<th><?=$th?></th>
						<?php endif; endforeach?>
					</tr>
				</thead>
				<tbody>
					<?php echo $body?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
	function month_filteroptions(){
		var filter = $('select#MONTH_FILTER').val();		
		location.href = '<?php echo $this->config->site_url('cms/'.strtolower($this->current_plugin).'/index')?>/'+filter+'/<?php echo $this->uri->segment(6)?>';
	}
	function year_filteroptions(){
		var filter_m = $('select#YEAR_FILTER').val();
		<?php $pais = ($this->uri->segment(4) == '')?'GUATEMALA':$this->uri->segment(4);?>	
		location.href = '<?php echo $this->config->site_url('cms/'.strtolower($this->current_plugin).'/index')?>/<?php echo $pais?>/'+filter_m;
	}
</script>
