<?php
/**
 * Plugin para administrar los contenidos editables del sitio
 * @author 	Guido A. Orellana
 * @name	Plugin_contenidos
 * @since	abril 2013
 *
 */
class Plugin_newsletter extends PL_Controller {

	function __construct() {
		parent::__construct();
		$this->load->helper('utilities');
		
		//Load the plugin data
		$this -> plugin_action_table 	= "PLUGIN_NEWSLETTER";
		$this -> plugin_button_create 	= "Enviar nuevo mensaje";
		$this -> plugin_button_cancel 	= "Cancelar";
		$this -> plugin_button_update 	= "Guardar Cambios";
		$this -> plugin_button_delete 	= "Eliminar";
		$this -> plugin_page_title 		= "Bolet&iacute;n Electr&oacute;nico";
		$this -> plugin_page_create 	= "Crear Bolet&iacute;n Electr&oacute;nico";
		$this -> plugin_page_read 		= "Mostrar Bolet&iacute;n Electr&oacute;nico";
		$this -> plugin_page_update 	= "Boletines Enviados";
		$this -> plugin_page_delete 	= "Eliminar";

		//Vistas de plugin
		$this -> template_display 		= "plugin_newsletter_display";
		$this -> template_update 		= "plugin_newsletter_update";
		
		
		//Extras to send
		$this->plugin_image_route		= "/user_files/uploads/images/";
		$this->image_max_width			= 530; //M�ximo ancho de la imagen
		$this->file_input				= "NEWSLETTER_IMAGE";
		$this->current_website			= $_SERVER['HTTP_HOST'];
		$this->company					= $this->fw_resource->request('RESOURCE_COMPANY_NAME');
		$this->tel						= $this->fw_resource->request('RESOURCE_COMPANY_PHONE');
		$this->contact_email			= $this->fw_resource->request('RESOURCE_CONTACT_EMAIL');
		$this->email_header				= $this->fw_resource->request('RESOURCE_EMAIL_IMAGE_ROUTE');
		$this->top_image_route			= base_url($this->email_header);

		$this -> plugin_display_array[0] = "ID";
		$this -> plugin_display_array[1] = "Asunto";
		$this -> plugin_display_array[2] = "Imagen";
		$this -> plugin_display_array[3] = "Receptor";
		$this -> plugin_display_array[4] = "Contenido";
		$this -> plugin_display_array[5] = "Fecha";
		

		$this -> plugins_model -> initialise($this -> plugin_action_table);

		//Extras to send
		$this->display_pagination			= TRUE; //Mostrar paginaci�n en listado
		$this->pagination_per_page			= 10; //Numero de registros por p�gina
		$this->pagination_total_rows		= $this->plugins_model->total_rows(); //Extras to send
		$display_filter						= ($this->uri->segment(4) == '')?date('m'):$this->uri->segment(4);
		$display_year						= ($this->uri->segment(5) == '')?date('Y'):$this->uri->segment(5);
		$this->base_url						= base_url('cms/'.strtolower($this->current_plugin).'/index/'.$display_filter.'/'.$display_year);
		$this->uri_segment					= 6;
		
		$this -> display_filter 			= 'LIST';
		
		$this->users_array 					= array(
												''			=> 'Todos los usuarios',
												'ENABLED' 	=> 'Distribuidores',
												'STANDBY' 	=> 'Aplicantes a distribuidor');
		
		
		$this->output->enable_profiler(FALSE);
	}	
	/**
	 * Funci�n para desplegar listado completo de datos guardados, enviar los t�tulos en array con clave header y el cuerpo en un array dentro de otro con clave body
	 *
	 * @param	$result_array 		array 		Array con la listado devuelto por query de la DB
	 * @return	$data_array 		array 		Arreglo con la informaci�n del header y body
	 */
	public function _html_plugin_display($result_array) {
		//DATOS de filtros
		$date_components_array				= date_components();
		$data_array['month_opts']			= $date_components_array['meses'];
		$data_array['years_opts']			= $date_components_array['aAnteriores'];
		$data_array['current_month']		= $result_array['filter']['month'];
		$data_array['current_year']			= $result_array['filter']['year'];
		
		
				
		//Header data
		$data_array['header'][1]			= $this->plugin_display_array[1];
		$data_array['header'][2]			= $this->plugin_display_array[5];
		$data_array['header'][3]			= $this->plugin_display_array[3];
		$data_array['header'][4]			= $this->plugin_display_array[4];
		
		$usuarios=$this->plugins_model->id_usuario ();
		
		//Body data
		$data_array['body'] = '';
		foreach($result_array['body'] as $field):
		$data_array['body']					.= '<tr>';
		$data_array['body']					.= '<td><a href="'.base_url('cms/'.strtolower($this->current_plugin).'/update_table_row/'.$field->ID).'">'.$field->NEWSLETTER_SUBJECT.'</a></td>';
		$data_array['body']					.= '<td>'.mysql_date_to_dmy($field->NEWSLETTER_DATE).'</a></td>';
		$data_array['body']					.= '<td>'.$this->users_array[$field->NEWSLETTER_RECIPIENTS].'</a></td>';
		$data_array['body']					.= '<td>'.character_limiter($field->NEWSLETTER_CONTENT, 90).'</td>';	
		$data_array['body']					.= '</tr>';
		
		
		endforeach;
		

		return $data_array;
		
	}
	/*
	 * Funci�n para crear nuevo contenido, desde aqu� se especifican los campos a enviar en el formulario.
	 * El formulario se env�a mediante objectos preestablecidos de codeigniter. 
	 * El formulario se env�a con un array con la clave form_html.
	 * Se puede encontrar una gu�a en: http://ellislab.com/codeigniter/user-guide/helpers/form_helper.html
	 */
	public function _html_plugin_create(){
        
		
		//Formulario
		$data_array['form_html']			 = "<div class='control-group'>".form_label($this->plugin_display_array[1],'',array('class' => 'control-label'))."<div class='controls'>".form_input(array('name' => 'NEWSLETTER_SUBJECT', 'class' => 'span6'))."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[2],'',array('class' => 'control-label'))."<div class='controls'>".form_upload(array('name' => 'NEWSLETTER_IMAGE', 'class' => 'span6'))."<span class='help-block'>ancho m�ximo ".$this->image_max_width."px</span></div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[3],'',array('class' => 'control-label'))."<div class='controls'>".form_dropdown('NEWSLETTER_RECIPIENTS', $this->users_array)."</div></div>";
		$data_array['form_html']			.= "<div class='control-group'>".form_label($this->plugin_display_array[4],'',array('class' => 'control-label'))."<div class='controls'>".form_textarea(array('name' => 'NEWSLETTER_CONTENT', 'class' => 'span6 textarea'))."<span class='help-block'>*No pegar texto directamente desde word</span></div></div>";
		return $data_array;
    }

	/*
	 * Funci�n para crear nuevo contenido, desde aqu� se especifican los campos a enviar en el formulario.
	 * El formulario se env�a mediante objectos preestablecidos de codeigniter.
	 * El formulario se env�a con un array con la clave form_html.
	 * Para enviar un formulario extra se agrega en el array la clave extra_form.
	 * Se puede encontrar una gu�a en: http://ellislab.com/codeigniter/user-guide/helpers/form_helper.html
	 */

	public function _html_plugin_update($result_data) {

		//Contenido enviado
		
		$data_array['form_html']  ='<dl class="dl-horizontal">';
		$data_array['form_html'] .="<dt>".($this -> plugin_display_array[3])."</dt><dd>".$this->users_array[$result_data -> NEWSLETTER_RECIPIENTS]."</dd>";
		$data_array['form_html'] .="<dt>".($this -> plugin_display_array[1])."</dt><dd>".ascii_to_entities($result_data -> NEWSLETTER_SUBJECT)."</dd>";
		$data_array['form_html'] .="<dt>".($this -> plugin_display_array[5])."</dt><dd>".mysql_date_to_dmy($result_data -> NEWSLETTER_DATE)."</dd>";
		$data_array['form_html'] .="</dl>";
		
		
		$data_array['form_html'] .='<div class="row-fluid"><div class="span1">&nbsp;</div>
							<div class="span5">
							<blockquote><p>Esta es una copia del mensaje enviado:</p></blockquote>
								<table width="550" style="background-color:#EEE;width:526px;font-size:12px; font-family:Arial, Helvetica; sans-serif;">
								<tr>
									<td>
										<table width="550" style="border-color:#dbdbdb;border-style:solid;border-width:1px;width:526px;font-size:12px; font-family:Arial, Helvetica; sans-serif;">
											<tr style="line-height:20px;"><td style="font-size:14px;"><img src="'.$this->top_image_route.'" width="550" height="60" style="padding:0px;margin:0px;" /></td></tr>';
			$data_array['form_html'] .= (!empty($result_data->NEWSLETTER_IMAGE))?'
											<tr>
												<td style="padding-left:10px;padding-top:10px;padding-bottom:10px"><img src="'.$result_data->NEWSLETTER_IMAGE.'" /></td>
											</tr>':'';
			$data_array['form_html'] .='
											<tr>
												<td style="padding-left:10px;padding-top:10px;padding-bottom:10px">'.ascii_to_entities($result_data->NEWSLETTER_CONTENT).'</td>
											</tr>
										</table>
									</td>
								</tr>
								</table>
								<p style="font-size:11px;color:#505050;">La informaci&oacute;n contenida en este mensaje es privada y confidencial. Si la ha recibido por error, por favor proceda a notificar al remitente y eliminarla de su sistema.</p>
								<p style="font-size:12px;color:#505050;">Atentamente,</p>
								<p style="font-size:12px;color:#505050;">'.$this->company.'</p>
								<p style="font-size:11px;color:#505050;">Tel. '.$this->tel.'</p>
								<p style="font-size:9px;color:#707070;">Custom Site desarrollado por <a href="http://www.grupoperinola.com">Perinola</a></p>
							</div>
							<div class="span6">&nbsp;</div>
							</div>';
		
		return $data_array;
	}

	/**
	 * Funciones para editar Querys o Datos a enviar desde cada plugin
	 */
	//Funci�n para desplegar listado, desde aqu� se puede modificar el query
	public function _plugin_display($filter){
		$offset 	= (isset($filter[2]))?$filter[2]:0;
		$listmeses	=(isset($filter[0]) && $filter[0] != 'display_all')?$filter[0]:date('m');
		$listanios	=(isset($filter[1]))?$filter[1]:date('Y');
		
				
		$this->pagination_total_rows = $this->plugins_model->total_rows("MONTH(NEWSLETTER_DATE)=".$listmeses." AND YEAR(NEWSLETTER_DATE)=".$listanios); //N�mero total de items a desplegar
		$result_array['body'] = $this->plugins_model->list_rows('', "MONTH(NEWSLETTER_DATE)=".$listmeses." AND YEAR(NEWSLETTER_DATE)=".$listanios, $this->pagination_per_page, $offset, 'ID DESC');
			
		$result_array['filter']['month'] 	= $listmeses;
		$result_array['filter']['year'] 	= $listanios;
		
		return $this ->_html_plugin_display($result_array);
		
		
	}
	
	//Funciones de los posts a enviar
	public function post_new_val() {
		$submit_posts 						= $this->input->post();
		$submit_posts['NEWSLETTER_DATE']	= date('Y-m-d');
		$addimg = $this->upload_image();
		$submit_posts['NEWSLETTER_IMAGE']	= ($addimg)?base_url($this->plugin_image_route.'/'.$addimg['file_name']):NULL;
		
		
		//Enviar el correo
		$users_data = $this->plugins_model->get_users_emails_by_status($submit_posts['NEWSLETTER_RECIPIENTS']);
		foreach($users_data as $user)
			$emails_array[] = $user->USER_EMAIL;
		
		$this->fw_posts->send_newsletter($emails_array, $submit_posts['NEWSLETTER_SUBJECT'], $submit_posts['NEWSLETTER_CONTENT'], $submit_posts['NEWSLETTER_IMAGE']);
		
		return $this -> _set_new_val($submit_posts);
	}

	public function post_update_val($data_id) {
		$submit_posts = $this -> input -> post();

		$submit_posts = array_map("entities_to_ascii", $submit_posts);

		return $this -> _set_update_val($submit_posts);
	}
	 
	/**
	 * Funciones espec�ficas del plugin
	 */
	
	/**
	 * Funciones espec�ficas del plugin
	 */
	 private function upload_image(){
	 	
			//Si se carga una imagen
			if(!empty($_FILES[$this->file_input]["name"])):
				$upload_config['upload_path'] 		= '.'.$this->plugin_image_route;
				$upload_config['overwrite']			= TRUE;
				$upload_config['allowed_types'] 	= 'gif|jpg|png';
				$upload_config['max_width']  		= $this->image_max_width;
				$this->upload->initialize($upload_config);
				
				if (!$this->upload->do_upload($this->file_input)):
					return false;
					$this->fw_alerts->add_new_alert(4002, 'ERROR');
				else:
					return $uploaded_data = $this->upload->data();
					$this->fw_alerts->add_new_alert(4001, 'SUCCESS');
				endif;
			endif;
	 }
	

}
